import * as path from "path"

import * as Express from "express"

import { ServeurBase } from "../infrastructure/serveur-base"

import { EmployeRepository, Employe } from "../domaine/employe.repository"

export class AppServeur extends ServeurBase {
    private employeRepository: EmployeRepository = new EmployeRepository()

    initialiserExpress(expressApp: Express.Application) {

        //Configurer le static
        expressApp.use(Express.static("./dist/client"))

        //Créer une personne
        expressApp.put("/personne", (req, res) => {
            let employeJson: domaine.IEmploye = req.body

            let employe = new Employe()
            employe.codePersonnel = employeJson.codePersonnel
            employe.nom = employeJson.nom
            employe.prenom = employeJson.prenom

            this.employeRepository.ajouterUnEmploye(employe)
            this.socketServer.emit("employeAjoute", employe)
        })
    }

    initialiserSocketIO(socketServer: SocketIO.Server) {

    }
}
