
import { createServer, Server } from 'http'
import * as path from 'path'

import * as Express from "express"
import * as bodyParser from "body-parser"
import * as IO from 'socket.io'

export abstract class ServeurBase {
    private serveur: Server

    expressApp: Express.Express
    socketServer: SocketIO.Server

    constructor(port: number = 3000) {
        this.expressApp = Express()

        this.expressApp.use(bodyParser.json()) // for parsing application/json
        this.expressApp.use(bodyParser.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

        this.expressApp.use('/assets', Express.static(path.join('./src', 'assets'), { maxAge: 30 }));
        this.expressApp.use(Express.static(path.join('./dist/client'), { index: false }));

        //Ajouter le header de restriction du NetScaler
        this.expressApp.use((req: Express.Request, res: Express.Response, next: Function) => {
            res.setHeader("X-UQAM-Restriction", "Secure");
            if (next) next()
        })

        //Express
        this.initialiserExpress(this.expressApp)

        //Tous ce qui n'est pas intercepté avant affichera 404 en json
        this.expressApp.get('*', function (req, res) {
            res.setHeader('Content-Type', 'application/json');
            var pojo = { status: 404, message: 'No Content' };
            var json = JSON.stringify(pojo, null, 2);
            res.status(404).send(json);
        });

        //SocketIO
        this.serveur = createServer(this.expressApp)
        this.socketServer = IO(this.serveur)
        this.initialiserSocketIO(this.socketServer)
    }

    abstract initialiserExpress(expressApp: Express.Express): void
    abstract initialiserSocketIO(socketServer: SocketIO.Server): void

    run() {
        this.serveur.listen(3000, function () {
            console.log("Go!")
        })
    }

}
