export class Personne implements domaine.IPersonne {
    nom: string
    prenom: string
    nomComplet(): string {
        return `${this.nom}, ${this.prenom}`
    }
}

export class Employe extends Personne implements domaine.IEmploye {
    codePersonnel: string
    toString(): string { return `[${this.codePersonnel}] ${super.nomComplet()}` }
}
