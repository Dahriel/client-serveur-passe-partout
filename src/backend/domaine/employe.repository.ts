import * as memoryCache from "memory-cache"
import * as _ from "underscore"

import { Employe } from "./employe"
export * from "./employe"

export class EmployeRepository {

    constructor() {
        memoryCache.put("repo", [])
    }

    ajouterUnEmploye(personne: Employe) {
        let repo: Employe[] = memoryCache.get("repo")
        repo.push(personne)
    }

    getEmploye(codePersonnel: string): Employe {
        let repo: Employe[] = memoryCache.get("repo")
        return _.findWhere(repo, { codePersonnel: codePersonnel })
    }

    getEmployes(): Employe[] {
        return memoryCache.get("repo")
    }

    supprimerEmploye(codePersonnel: string) {
        let repo: Employe[] = memoryCache.get("repo")
        let e = this.getEmploye(codePersonnel)
        _.without(repo, e)
    }
}
