
declare namespace domaine {
    export interface IPersonne {
        nom: string
        prenom: string
    }

    export interface IEmploye extends IPersonne {
        codePersonnel: string
    }
}


