import * as io from "socket.io-client"

export class App {
    private inc: number
    private socket: SocketIOClient.Socket
    constructor() {
        this.inc = 10
        this.socket = io("/")

        this.socket.on("coucou", (message: string) => { console.log("Socket : ", message) })
    }

    incrementer() {
        this.inc++
    }

    toString() { return `Inc : ${this.inc}` }
}
