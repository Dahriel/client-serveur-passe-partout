var path = require('path');

var webpack = require('webpack');

var helpers = require('./helpers');

var HtmlWebpackPlugin = require('html-webpack-plugin');

var commonConfig = {
    output: {
        filename: 'index.js'
    },
    resolve: {
        extensions: ['.ts', '.js', '.json']
    },
    module: {
        loaders: [
            // TypeScript
            { test: /\.ts$/, loaders: ['awesome-typescript-loader'] },
            { test: /\.json$/, loader: 'json-loader' }
        ],
    },
    plugins: [

    ]
};

/*var clientVendorConfig = {
    target: 'web',
    //devtool: "#cheap-source-map", //for sourcemap
    devtool: "#eval", //for speed
    entry: './src/client.vendor',
    output: {
        path: helpers.root('dist', 'client', 'vendor')
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin({
            name: ['client', 'vendor']
        })
    ]
};*/

var ExtractTextPlugin = require("extract-text-webpack-plugin");
var clientConfig = {
    target: 'web',
    //devtool: "#cheap-source-map", //for sourcemap
    devtool: "#eval", //for speed
    resolve: {
        extensions: ['.scss']
    },
    entry: './src/client',
    output: {
        path: 'dist/client',
        filename: 'index_bundle.js'
    },
    module: {
        loaders: [
            { test: /\.html$/, loader: 'raw-loader' },
            { test: /\.scss$/, loader: ExtractTextPlugin.extract("style-loader", "css-loader!sass-loader") }
        ]
    },
    plugins: [
        new ExtractTextPlugin("[name].css"),
        new HtmlWebpackPlugin({
            template: helpers.root('src', 'index.html')
        })
    ]
};

var serverConfig = {
    target: 'node',
    devtool: "#source-map",
    entry: './src/serveur',
    externals: [
        helpers.checkNodeImport
    ],
    output: {
        path: helpers.root('dist', 'serveur'),
        libraryTarget: 'commonjs2'
    },
    node: {
        global: true,
        __dirname: true,
        __filename: true,
        process: true,
        Buffer: true
    }
};

var webpackMerge = require('webpack-merge');
module.exports = [
    // Client
    webpackMerge({}, commonConfig, clientConfig),

    // Server
    webpackMerge({}, commonConfig, serverConfig)
];
